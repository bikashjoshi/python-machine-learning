import numpy as np
import datetime as dt
from sklearn import svm

class xAndYArrays:
    def __init__(self, x, Y):
        self.x = x
        self.Y = Y

    @staticmethod
    def fileTo_xAndYArrays(filePath, delimiter=','):
        arrays = np.genfromtxt(filePath, delimiter=delimiter)
        arrays = arrays[1:]
        x = arrays[:, [0]]
        x = np.ravel(x)
        Y = np.delete(arrays, 0, 1)
        return xAndYArrays(x, Y)

print("----------------------------------")
start = dt.datetime.now()

trainings = xAndYArrays.fileTo_xAndYArrays("C:\\Users\\bjoshi\\Desktop\\Python_Samples\\machinelearning\\trainingsample.csv")
end = dt.datetime.now()
print("Training samples loaded. Time elapsed {0} so far.".format(end - start))

clf = svm.SVC(kernel="poly", gamma=0.01, C=100)
clf.fit(trainings.Y, trainings.x)
end = dt.datetime.now()
print("Classifier trained. Time elapsed {0} so far.".format(end - start))

total = 0
hits = 0
end = dt.datetime.now()
validations = xAndYArrays.fileTo_xAndYArrays("C:\\Users\\bjoshi\\Desktop\\Python_Samples\\machinelearning\\validationsample.csv")
print("Validation samples loaded. Time elapsed {0} so far.".format(end - start))

for i, Y in enumerate(validations.Y):
    actual = validations.x[i]
    prediction = clf.predict(Y.reshape(1, -1))
    estimation = np.asscalar(prediction)
    matched = actual == estimation
    statusText = "MISSED"
    if matched:
        hits += 1
        statusText = "MATCHED"
    total += 1
    print("Estimated {0} for actual {1} for sample {2}".format(estimation, actual, total, statusText))

end = dt.datetime.now()
print("Accuracy: {0}, Hits: {1}, Misses: {2}, Total: {3}".format(str(hits * 100.0 / total), hits, total - hits, total))
print("Total time taken for analysis: {0}".format(end - start))
