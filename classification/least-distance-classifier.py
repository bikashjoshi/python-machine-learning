import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import sys

class PixelLabel:
    def __init__(self, label, pixels):
        self.label = label
        self.pixels = pixels

    @staticmethod
    def fileToPixelLabel(filePath, delimiter=','):
        arrays = np.genfromtxt(filePath, delimiter=delimiter)
        pixelLabels = []
        for line in arrays[1:]:
            length = len(line)
            n = (length -1) ** 0.5
            m = int(n)
            if m != n:
                print(n)
                raise "Not a square matrix."
            pixelLabel = PixelLabel(line[0], line[1:].reshape(m, m))
            pixelLabels.append(pixelLabel)
        return pixelLabels

    def draw(self):
        plt.imshow(self.pixels, aspect="auto")
        plt.show()

class Classifier:
    def __init__(self, trainingPixelLabels):
        self.trainingPixelLabels = trainingPixelLabels

    def estimate(self, pixels):
        d = sys.float_info.max
        e = -1
        for trainingPixelLabel in self.trainingPixelLabels:
            dist = Classifier.distance(trainingPixelLabel.pixels, pixels)
            if dist < d:
                d = dist
                e = trainingPixelLabel.label
        if e == -1:
            raise "Not found.."
        return e

    @staticmethod
    def distance(p1, p2):
        length = len(p1)
        if length != len(p2):
            raise "Two matrix must be of same dimension."
        diff = p1 - p2
        square = np.power(diff, 2)
        sum = np.sum(square)
        return sum ** 0.5

def validate(trainingSampleFile, validationFile):
    start = dt.datetime.now()

    print("loading training data for classfier..")
    classifier = Classifier(PixelLabel.fileToPixelLabel(trainingSampleFile))
    end = dt.datetime.now()
    print("Classifier initialized. Took {0} to load.".format(end-start))
    validationData = PixelLabel.fileToPixelLabel(validationFile)
    print("Validation data loaded..")

    print("Estimating and comparing validation data using classifier..")
    total = 0
    hits = 0
    for data in validationData:
        estimation = classifier.estimate(data.pixels)
        actual = data.label
        statusText = "MISSED"
        matched = actual == estimation
        if matched:
            hits += 1
            statusText = "MATCHED"
        total += 1
        print("Estimated {0} for actual {1} for sample {2} -- {3}".format(estimation, actual, total, statusText))
        #if not matched:
            #data.draw()

    end = dt.datetime.now()
    print("Accuracy: {0}, Hits: {1}, Misses: {2}, Total: {3}".format(str(hits * 100.0 / total), hits, total - hits, total))
    print("Total time taken for analysis: {0}".format(end - start))

if __name__ == "__main__":
    validate(trainingSampleFile="trainingsample.csv", validationFile="validationsample.csv")







