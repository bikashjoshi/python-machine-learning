import numpy as np
import datetime as dt
import sys

class xAndYArrays:
    def __init__(self, x, Y):
        self.x = x
        self.Y = Y

    @staticmethod
    def fileTo_xAndYArrays(filePath, delimiter=','):
        arrays = np.genfromtxt(filePath, delimiter=delimiter)
        arrays = arrays[1:]
        x = arrays[:, [0]]
        Y = np.delete(arrays, 0, 1)
        shape = Y.shape
        rows = shape[0]
        columns = shape[1]
        n = columns ** 0.5
        Y = Y[:].reshape(rows, n, n)
        print(Y.shape)
        return xAndYArrays(x, Y)

class Classifier:
    def __init__(self, training_xAndYArrays):
        self.training_xAndYArrays = training_xAndYArrays

    def estimate(self, pixels_samples):
        estimation = []
        for pixels in pixels_samples:
            d = sys.float_info.max
            index = -1
            for i,Y in enumerate(self.training_xAndYArrays.Y):
                dist = Classifier.distance(Y, pixels)
                if dist < d:
                    d = dist
                    index = i
            if index == -1:
                raise "Not found.."
            estimation.append(self.training_xAndYArrays.x[index])
        return np.array(estimation)

    @staticmethod
    def distance(p1, p2):
        diff = p1 - p2
        square = np.power(diff, 2)
        sum = np.sum(square)
        return sum ** 0.5

def hiliteText(string, status):
    attr = []
    if status:
        # green
        attr.append('32')
    else:
        # red
        attr.append('31')
    return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)

def validate(trainingSampleFile, validationFile):
    start = dt.datetime.now()

    print("loading training data for classfier..")
    classifier = Classifier(xAndYArrays.fileTo_xAndYArrays(trainingSampleFile))
    end = dt.datetime.now()
    print("Classifier initialized. Took {0} to load.".format(end-start))
    validationData = xAndYArrays.fileTo_xAndYArrays(validationFile)
    print("Validation data loaded..")

    print("Estimating and comparing validation data using classifier..")

    estimations = classifier.estimate(validationData.Y)
    actuals = validationData.x
    comparision = estimations == actuals
    matches = np.sum(comparision)
    total = comparision.shape[0]

    end = dt.datetime.now()
    print("Accuracy: {0}, Hits: {1}".format(str(matches * 100.0 / total), matches))
    print("Total time taken for analysis: {0}".format(end - start))


if __name__ == "__main__":
    validate(trainingSampleFile="trainingsample.csv", validationFile="validationsample.csv")






